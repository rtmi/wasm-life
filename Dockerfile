# build stage
FROM golang:alpine AS build-env
ENV GOOS=js \
    GOARCH=wasm 

COPY . /go/src/patterns/wasm

RUN cd /go/src/patterns/wasm \
    && go build -o www/test.wasm www/main.go


# final stage
FROM node:alpine
COPY --from=build-env /go/src/patterns/wasm/www/* /usr/local/wasm/
RUN npm install http-server -g
WORKDIR /usr/local/wasm
ENTRYPOINT ["http-server"]

