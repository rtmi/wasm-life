// Trying to Go-ify the Rust & Webassembly tutorial
// see https://rustwasm.github.io/book/game-of-life/implementing.html
//

package main

import (
	"errors"
	"fmt"
	"strconv"
	"syscall/js"

	"patterns/wasm/life"
)

var (
	beforeUnloadCh = make(chan struct{})
	uni            *life.Universe
)

const CANVAS_ID = "drawing"

func main() {
	callback := js.NewCallback(printLife)
	defer callback.Release()
	setPrintLife := js.Global().Get("setPrintLife")
	setPrintLife.Invoke(callback)

	beforeUnloadCb := js.NewEventCallback(0, beforeUnload)
	defer beforeUnloadCb.Release()
	addEventListener := js.Global().Get("addEventListener")
	addEventListener.Invoke("beforeunload", beforeUnloadCb)

	<-beforeUnloadCh
	fmt.Println("Done")
}

func printLife(args []js.Value) {
	if uni == nil {
		n, err := extractArg(args)
		if err != nil {
			fmt.Println("Dimension size is required")
			return
		}
		uni = life.NewUniverse(n)
		uni.SizeGrid(CANVAS_ID)
	}

	uni.DrawGrid(CANVAS_ID)
	uni.DrawCells(CANVAS_ID)
	uni.Tick()
	raf := js.Global().Get("rafCb")
	raf.Invoke()
}

func beforeUnload(event js.Value) {
	beforeUnloadCh <- struct{}{}
}

func extractArg(args []js.Value) (int, error) {
	if len(args) == 0 {
		return 0, errors.New("Arg required")
	}
	a := args[0].String()
	n, err := strconv.Atoi(a)
	if err != nil {
		return 0, errors.New("Arg required")
	}
	return n, nil
}
