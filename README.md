# wasm-life
This follows the Rust &amp; WebAssembly book.
 It&apos;s adjusted for the WASM target supported in Go 1.11

## Quickstart
1. git clone https://github.com/patterns/wasm-life
2. docker build --network=host -t life wasm-life
3. docker run -ti --rm --network=host life
4. Visit localhost:8080 with the browser and load wasm_exec.html


## Credits
Go 1.11 WebAssembly article by
 [Nicolas Lepage](https://medium.zenika.com/go-1-11-webassembly-for-the-gophers-ae4bb8b1ee03)

Rust &amp; WebAssembly
 [book](https://rustwasm.github.io/book/game-of-life/implementing.html)

